<?php

namespace CycleMarket;

class DataBase
{
    const HOST = 'localhost';

    const USERNAME = 'root';

    const PASSWORD = '8970Nd2144!';

    const DATABASENAME = 'db_cycle_market';

    const NAMESPACE = 'CycleMarket\\';

    private $conn;

    function __construct()
    {
        $this->conn = $this->getConnection();
    }

    public function getConnection()
    {
        try{
            if(!empty(getenv("CLEARDB_DATABASE_URL"))){
                $url = parse_url(getenv("CLEARDB_DATABASE_URL"));
                $server = $url["host"];
                $username = $url["user"];
                $password = $url["pass"];
                $db = substr($url["path"], 1);
                $conn = new \PDO('mysql:host=' . $server . ';dbname=' . $db . ';charset=utf8', $username, $password, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
            }else{
                $conn = new \PDO('mysql:host=' . self::HOST . ';dbname=' . self::DATABASENAME . ';charset=utf8', self::USERNAME, self::PASSWORD, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
            }
        }
        catch (\Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
        return $conn;
    }

    public function select_row($query, $params=array())
    {   
        $smth = $this->conn->prepare($query);
        $smth->execute($params); 
        if($smth !== false){
            $smth = $smth->fetchAll();
        }
        return $smth;
    }

    public function select_class($query, $fetchClass, $params=array())
    {   
        $smth = $this->conn->prepare($query);
        $smth->execute($params); 
        if($smth !== false){
            $smth = $smth->fetchAll(\PDO::FETCH_CLASS, self::NAMESPACE . $fetchClass);
        }
        return $smth;
    }

    public function select_one_class($query, $fetchClass, $params=array())
    {   
        $smth = $this->conn->prepare($query);
        $smth->execute($params); 
        if($smth !== false){
            $smth->setFetchMode(\PDO::FETCH_CLASS, self::NAMESPACE . $fetchClass);
            $smth = $smth->fetch();
        }
        return $smth;
    }
    
    public function insert($query, $paramArray=array(), $paramType=array())
    {
        $this->execute($query, $paramArray, $paramType);
        return $this->conn->lastInsertId();
    }
    
    public function execute($query, $paramArray=array(), $paramType=array())
    {
        $smth = $this->conn->prepare($query);
        if(!empty($paramType) && !empty($paramArray)) {
            $this->bindQueryValues($smth, $paramArray, $paramType);
        }
        $smth->execute();
        $smth->closeCursor();
        return $smth;
    }
    
    public function bindQueryParams($smth, $paramName=array(), $paramArray=array())
    {
        for ($i = 0; $i < count($paramArray); $i ++) {
            $smth->bindParam($paramName[$i], $paramArray[$i]);
        }
    }
    
    public function bindQueryValues($smth, $paramArray=array(), $paramType=array())
    {
        for ($i = 0; $i < count($paramArray); $i ++) {
            $smth->bindValue($i, $paramArray[$i], $paramType[$i]);
        }
    }
   
}