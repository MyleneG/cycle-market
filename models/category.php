<?php

namespace CycleMarket;

class Category{
    private int $id;
    private string $name;
    private string $icon;

    function __construct()
    {
        return $this;
    }

    /*          Méthodes des attributs           */
    function getId()
    {
        return $this->id;
    }
    
    function getName()
    {
        return $this->name;
    }
    
    function getIcon()
    {
        return $this->icon;
    }

    /*          Méthodes d'actions            */
    public static function getAll()
    {
        $db = new DataBase;
        $query = "SELECT * FROM `category`";
        return $db->select_class($query, 'Category');
    }

    public static function getById(int $id)
    {
        $db = new DataBase;
        $query = "SELECT * FROM `category` where id = ?";
        return $db->select_one_class($query, 'Category', [$id]);
    }
    
    public static function create(string $name, string $icon){
        $db = new DataBase;
        $req = "INSERT INTO `category` (`name`, `icon`) 
                VALUES ('$name', '$icon')";
        $res = $db->insert($req);
        return $res;
    }

    public static function update(int $id, string $name, string $icon)
    {
        $db = new DataBase;
        $req = "UPDATE `category` SET `name`='". $name . "' `icon`='" . $icon . "' WHERE id=" . $id;
        $res = $db->execute($req);
        return $res;
    }

    public static function deleteById(int $id)
    {
        $db = new DataBase;
        $query = "DELETE FROM `category` where id = " . $id;
        return $db->execute($query);
    }
}

