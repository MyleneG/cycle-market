# cycle-market
## Run the project
Only files inside public/ folder are accessible when the project is on, by running :
```
php -S localhost:8000 -t public
```

## Good practice
### Git
Always remember to pull the main branch and merge it to your local branch before pushing and creating a pull request. 
```
git add <your_file|your_folder|*>
git commit -m <your_message>
git checkout main
git pull
git checkout <your_local_branch>
git merge main <your_local_branch>

    *************************************
    *** Resolve conflics if there are ***
    *************************************

git push origin <your_local_branch>
```

### Functions
Respect the format camel case when writting functions.
```
yourFunction();
```

### Models

The autoload can only works with models that goes in one word, so :
```
models/subcategory.php #will work
models/sub_category.php #will not work
```

## Style
### SCSS
Style is writted in SCSS first. To compile the result to CSS, run the command (from the root directory):
```
    sass --watch public/css/app.scss:public/css/app.css
```

### Icons
Material Icons (https://fonts.google.com/icons) provides a full icon library with 5 different themes. All these icons are enable inside the project just by including this Google Font link :

```
<link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
```
Icon insertion is made as following :

```
<span class="material-icons-outlined">
    icon_name
</span>
```
Full documentation here : https://developers.google.com/fonts/docs/material_icons.