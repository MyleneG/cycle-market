<?php

function redirect_account (): void {
  if(is_connected()){
    session_write_close();
    if($_SERVER['SERVER_NAME'] == "localhost"){
        $url = "http://" . $_SERVER['HTTP_HOST'];
    }else{
        $url = "https://" . $_SERVER['SERVER_NAME'];
    }
    header("Location: " . $url . "/mon-compte");
    exit;
  }
}

function redirect_unconnected_user (): void {
  if(!is_connected()){
    session_write_close();
    if($_SERVER['SERVER_NAME'] == "localhost"){
        $url = "http://" . $_SERVER['HTTP_HOST'];
    }else{
        $url = "https://" . $_SERVER['SERVER_NAME'];
    }
    header("Location: " . $url . "/connexion");
    exit;
  }
}

function redirect_connected_user (): void {
  if(is_connected()){
    session_write_close();
    if($_SERVER['SERVER_NAME'] == "localhost"){
        $url = "http://" . $_SERVER['HTTP_HOST'];
    }else{
        $url = "https://" . $_SERVER['SERVER_NAME'];
    }
    header("Location: " . $url);
    exit;
  }
}