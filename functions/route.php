<?php

function loginFunc(): void{
  $errorMessage = array();
  $mail = $_POST['current-mail'] ?? '';
  $password = $_POST['current-password'] ?? '';
  if(!empty($_POST['current-mail']) && !empty($_POST['current-password'])) {
    if(isMailExists($mail)){
      if(isMailMatchsPassword($mail, $password)){
        $user = getUserByMail($mail);
        $_SESSION["current-user-id"] = $user->getId();
        $_SESSION["current-basket"] = Array();
        $_SESSION["current-address-id"] = -1;
        $_SESSION["current-payment-id"] = -1;
        $GLOBALS['validationMessage'] = 'Bienvenue ' . $user->getFirstname();
      }else{
        $errorMessage[] = "Le mot de passe ne correspond pas à votre adresse mail.";
      }
    }
    else{
      $errorMessage[] = "Cette adresse mail n'est pas attribuée.";
    }
  }
  if(isset($_SESSION["current-user-id"])){
    redirect_connected_user();
  }
}

function registerFunc(): void {
  $error = validateMember();
  if(!empty($_POST) && is_null($error) || !empty($_POST) && !is_string($error)){
    $id = createUser($_POST["user-firstname"], $_POST["user-lastname"], $_POST["user-mail"], $_POST["user-password"], null, 
      null, $_POST["user-address"], $_POST["user-additional-address"], $_POST["user-postcode"], $_POST["user-town"]);
    if(isset($id)){
      if(session_status() === PHP_SESSION_NONE){
        session_start();
      }
      $_SESSION["current-user-id"] = $id;
      if(!isset($_SESSION["current-basket"])){
        $_SESSION["current-basket"] = Array();
      }
      $GLOBALS['validationMessage'] = 'Votre compte a été créé avec succés.';
    }
  }
  if(isset($_SESSION["current-user-id"])){
    redirect_connected_user();
  }
}
