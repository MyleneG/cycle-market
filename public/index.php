<?php

require '../vendor/autoload.php';
require '../controllers/imports.php';
require '../functions/imports.php';

session_start();

define('IMAGE_PATH', DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'images');
define('VIEW_PATH', DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('CSS_PATH', DIRECTORY_SEPARATOR . 'css');
define('SCRIPT_PATH', DIRECTORY_SEPARATOR . 'scripts');
define('CSS_FILE', CSS_PATH . DIRECTORY_SEPARATOR . 'app.css');

$path = __DIR__ . DIRECTORY_SEPARATOR . 'views';
$router = new CycleMarket\Router($path);

$router->get('/', 'home', 'home')
		->get('/catergorie/[i:subcategory]/', 'subcategory', 'subcategory')
		->getPost('/inscription', 'register', 'register')
		->getPost('/connexion', 'login', 'login')
		->get('/mon-compte', 'account', 'account')
		->getPost('/ajouter-une-annonce', 'addannounce', 'addannounce')
		->get('/logout', 'logout', 'logout')
		->run();

$match=$router->match();
