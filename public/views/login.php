<?php
  loginFunc();
?>
<main>
  <?php require "layout" . DIRECTORY_SEPARATOR . "messages.php"; ?>
  <form id="form" action=<?= $router->generate('login')?> method="POST" class="without-label">
    <h2>
      Connectez-vous
    </h2>
    <input type="email" name="current-mail" placeholder="Votre adresse mail" autocomplete="on" />
    <input type="password" name="current-password" placeholder="Votre mot de passe" autocomplete="on" />
    <a id="register" class="tertiary" href=<?= $router->generate('register')?>>
      Pas encore inscrit ?
    </a>
    <button type="submit">
      Se connecter
    </button>
  </form>
</main>