<?php
  registerFunc();
?>
<main>
 <?php require "layout" . DIRECTORY_SEPARATOR . "messages.php"; ?>
  <form id="form" action=<?= $router->generate('register')?> method="POST">
    <h2>
      Inscription
    </h2>
    <label for="input-firstname">Prénom :</label>
    <input type="text" name="user-firstname" placeholder="Votre prénom" autocomplete="off" id="input-firstname" required/>
    <label for="input-lastname">Nom :</label>
    <input type="text" name="user-lastname" placeholder="Votre nom de famille" autocomplete="off" id="input-lastname" required/>
    <label for="input-mail">Email :</label>
    <input type="email" name="user-mail" placeholder="Votre adresse mail" autocomplete="off" id="input-mail" required/>
    <label for="input-password">Mot de passe :</label>
    <div>
      <input type="password" name="user-password" placeholder="Votre mot de passe" autocomplete="off" id="input-password" required/>
      <input type="password" name="user-password-confirm" placeholder="Confirmez votre mot de passe" autocomplete="off" required/>
    </div>
    <label for="input-address">Adresse :</label>
    <div>
        <input type="text" name="user-address" placeholder="Votre adresse" autocomplete="on"  id="input-address" required/>
        <input type="text" name="user-additional-address" placeholder="Complément d'adresse si besoin" autocomplete="on"/>
        <input type="number" name="user-postcode" placeholder="Code postal" autocomplete="on" required/>
        <input type="text" name="user-town" placeholder="Ville" autocomplete="on" required/>
    </div>
    <div class="terms">
      <input type="checkbox" name="user-terms"> J'accepte les conditions générales d'utilisation.
    </div>
    <a id="login" class="tertiary" href=<?= $router->generate('login')?>>
      Déjà inscrit ?
    </a>
    <button type="submit">
      S'inscrire
    </button>
  </form>
</main>