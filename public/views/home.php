<?php
    $annonces = getAnnonces();
?>
<main>
    <div class="flex">
        <?php foreach ($annonces as $a) : ?>
            <div class="annonce-card grid just-it-center">
                <img src=<?= IMAGE_PATH . DIRECTORY_SEPARATOR . "not-found.png" ?>>
                <h5>
                    <?= $a->getName() ?>
                </h5>
                <span>
                    <?= $a->getDescription() ?>
                </span>
                <span class="status-<?= $a->getStatusId() ?>">
                    Disponible
                </span>
            </div>
        <?php endforeach ?>
    </div>
</main>