<?php
    $cat = getCategories();
    $sub = getSubCategories();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href=<?= CSS_FILE ?> />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
    <script type="text/javascript" src=<?= SCRIPT_PATH . DIRECTORY_SEPARATOR . 'app.js' ?> ></script>
    <title>Cycle Market</title>
</head>
<body class="mn-0">
    <header class="pos-fixed width-100pc top-0 z-15">
        <div class="mobile-media header grid auto-col-mc just-ct-sb gap-24px al-it-center">
            <a href=<?= $router->generate('home')?>>
                <img id="logo" src="<?= IMAGE_PATH . DIRECTORY_SEPARATOR . "cycle_market_vn.svg" ?>" />
            </a>
            <span id="menu" class="material-icons">
                menu
            </span>
        </div>
        <div class="tv-media header grid auto-col-mc just-ct-sb gap-24px al-it-center">
            <a href=<?= $router->generate('home')?>>
                <img id="logo" src="<?= IMAGE_PATH . DIRECTORY_SEPARATOR . "cycle_market_vn.svg" ?>" />
            </a>
            <form id="search" method="GET" action="#" class="width-mc">
                <input name="search" type="text"/>
                <button type="submit">Submit</button>
            </form>
            <?php if(isConnectedUser()) : ?>
                <div class="grid auto-col-mc al-it-center gap-4px">
                    <div><span>Bonjour</span> <span class="current-username"><?= getCurrentUser()->getFirstName() ?></span></div>
                    <div class="current-usermenu">
                        <img src=<?= IMAGE_PATH . DIRECTORY_SEPARATOR . "user-profile.webp" ?> class="current-userpicture">
                        <ul id="hidden-menu" class="width-mc mn-0 text-center">
                            <li>
                                <span>
                                    <a href=<?= $router->generate('account')?>>
                                        Mon compte
                                    </a>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <a href=<?= $router->generate('logout')?>>
                                        Se déconnecter
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php else : ?>
                <div>
                    <span>
                        <a href=<?= $router->generate('register')?>>
                            Inscription
                        </a> / <a href=<?= $router->generate('login')?>>
                            Connexion
                        </a>
                    </span>
                </div>
            <?php endif ?>
        </div>
        <nav>
            <ul class="categories mn-0 pg-0">
                <?php foreach ($cat as $c) : ?>
                    <li class="categories__li">
                        <span><?= $c->getName() ?></span>
                        <div class="sub-categories width-mc pos-fixed z-1">
                            <ul>
                                <?php foreach ($sub as $s) : ?>
                                    <li class="sub-categories__li">
                                        <?= $s->getName() ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </li>
                <?php endforeach ?>
            </ul>
        </nav>
    </header>
    <?= $content ?>
    <a href=<?= $router->generate('addannounce')?> id="add-annonce" class="grid auto-col-mc width-mc al-it-center">
        <span class="material-icons">
            add
        </span>
    </a>
</body>
<footer>
</footer>
</html>