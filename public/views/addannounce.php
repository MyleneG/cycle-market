<?php
addAnnonceFunc();
    $cat = getCategories();
    $user = getCurrentUser();
    $sub = getSubCategories();
    require "layout" . DIRECTORY_SEPARATOR . "messages.php";
?>
<main>
	<section id="announce" class="content current">
        <a class="material-icons-outlined" href="<?= $router->generate('home')?>">
            arrow_back_ios
        </a>
        <form id="form" action=<?= $router->generate('addannounce')?> method="POST" class="pos-rel">
            <div id="form-choice-category">
                <?php foreach ($cat as $c) : ?>
                    <input type="radio" id="category<?= $c->getId() ?>" class="checkbox-tools" name="category" value="<?= $c->getId() ?>" required>
                    <label for="category<?= $c->getId() ?>">
                        <span class="material-icons-outlined uil uil-line-alt">
                        <?= $c->getIcon() ?>
                        </span>
                        <?= $c->getName() ?>
                    </label>
                <?php endforeach ?>
            </div>
            <label for="input-name">Nom :</label>
            <input type="text" name="name" value="" placeholder="Le nom de l'annonce" autocomplete="off"  id="input-name" required/>
            <label for="input-description">Description :</label>
            <textarea name="description" placeholder="Description" autocomplete="off" minlength="10" id="input-description" required></textarea>
            <label for="input-sub_category">Catégorie :</label>
            <select id="input-sub_category" name="sub_category">                    
                <?php foreach ($sub as $s) : ?>
                    <option value="<?= $s->getId() ?>">
                        <?= $s->getName() ?>
                    </option>
                <?php endforeach ?>
            </select>
            <label for="input-deposit">Garantie :</label>
            <div class="pos-rel width-100pc">
                <input type="number" name="deposit" value="" placeholder="Le montant de la garantie" autocomplete="off" id="input-deposit" required/>
                <span class="unit pos-abs">€</span>
            </div>
            <button type="submit" name="submit" value="addannounce">
                Enregrister
            </button>
        </form>
    </section>
</main>