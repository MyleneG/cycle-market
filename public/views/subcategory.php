<?php
    $current_sub = getSubCategoryByName($params['subcategory']);
    $annonces = getAnnoncesBySubCategoryId($current_sub->getId());
?>
<div class="annonces">
    <?php foreach ($annonces as $a) : ?>
        <div class="annonce__preview">
            <h5 class="annonce__title"></h5>
            <img class="annonce__img" src=""/>
            <span class="annonce__desc"></span>
        </div>
    <?php endforeach?>
</div>