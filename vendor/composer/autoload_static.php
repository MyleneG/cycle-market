<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd0e34b828f396511a3f3aa00e23df15f
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Functions\\' => 10,
        ),
        'C' => 
        array (
            'CycleMarket\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Functions\\' => 
        array (
            0 => __DIR__ . '/../..' . '/controllers',
        ),
        'CycleMarket\\' => 
        array (
            0 => __DIR__ . '/../..' . '/models',
        ),
    );

    public static $classMap = array (
        'AltoRouter' => __DIR__ . '/..' . '/altorouter/altorouter/AltoRouter.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd0e34b828f396511a3f3aa00e23df15f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd0e34b828f396511a3f3aa00e23df15f::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitd0e34b828f396511a3f3aa00e23df15f::$classMap;

        }, null, ClassLoader::class);
    }
}
